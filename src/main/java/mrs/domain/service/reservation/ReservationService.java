package mrs.domain.service.reservation;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mrs.domain.mapper.ReservableRoomMapper;
import mrs.domain.mapper.ReservationMapper;
import mrs.domain.model.Reservation;

@Service
@Transactional
public class ReservationService {

	@Autowired
	ReservationMapper reservationMapper;
	
	@Autowired
	ReservableRoomMapper reservableRoomMapper;
	
	public List<Reservation> findReservations(Integer roomId, LocalDate reservedDate) {
		return reservationMapper
				.findByRoomIdAndReservedDateOrderByStartTimeAsc(
						roomId, reservedDate);
	}
}
