package mrs.domain.mapper;

import java.time.LocalDate;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import mrs.domain.model.ReservableRoom;

@Mapper
public interface ReservableRoomMapper {

	public List<ReservableRoom> findByReservedDateOrderByRoomIdAsc(LocalDate reservedDate);
}
