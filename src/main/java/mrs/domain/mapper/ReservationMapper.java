package mrs.domain.mapper;

import java.time.LocalDate;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import mrs.domain.model.Reservation;

@Mapper
public interface ReservationMapper {

	public Reservation findOne(Integer reservationId);
	
	public List<Reservation> findByRoomIdAndReservedDateOrderByStartTimeAsc(Integer roomId, LocalDate reservedDate);
}
