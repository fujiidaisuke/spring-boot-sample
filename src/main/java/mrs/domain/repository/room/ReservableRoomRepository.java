package mrs.domain.repository.room;

import java.time.LocalDate;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import mrs.domain.model.ReservableRoom;

@Mapper
public interface ReservableRoomRepository {
	
	@Select("select room_id as roomId, reserved_date as reservedDate from reservable_room order by room_id asc")
	List<ReservableRoom> findByreservedDateOrderByRoomIdAsc(LocalDate reservedDate);

}
