package mrs.domain.model;

import java.io.Serializable;
import java.time.LocalTime;

public class Reservation implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2299874295313684115L;

	private Integer reservationId;
	
	private LocalTime startTime;
	
	private LocalTime endTime;
	
	private ReservableRoom reservableRoom;
	
	private User user;

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public ReservableRoom getReservableRoom() {
		return reservableRoom;
	}

	public void setReservableRoom(ReservableRoom reservableRoom) {
		this.reservableRoom = reservableRoom;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
