package mrs.domain.model;

import java.io.Serializable;
import java.time.LocalDate;

public class ReservableRoom implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4375167871276853186L;
	
	private Integer roomId;
	
	private LocalDate reservedDate;

	private MeetingRoom meetingRoom;
	
	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public LocalDate getReservedDate() {
		return reservedDate;
	}

	public void setReservedDate(LocalDate reservedDate) {
		this.reservedDate = reservedDate;
	}

	public MeetingRoom getMeetingRoom() {
		return meetingRoom;
	}

	public void setMeetingRoom(MeetingRoom meetingRoom) {
		this.meetingRoom = meetingRoom;
	}
}
